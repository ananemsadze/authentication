package com.example.authenticationactivity

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_sign_up.*


class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()


    }

    private fun init() {
        auth = Firebase.auth
        signbutton.setOnClickListener{
            signup()
        }

    }
    private fun emailchecker() {
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(emailedittext.text.toString()).matches())

        else{ emailedittext.setError("Email format is not correct")
        }
    }


    private fun signup() {
        emailchecker()
        val email = emailedittext.text.toString()
        val password = passwordedittext.text.toString()
        val repeatpassword = passwordrepeatedittext.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty() && repeatpassword.isNotEmpty()) {
            if(password == repeatpassword)
            {signbutton.isClickable = false
                progressbar.visibility = View.VISIBLE
                auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this) { task ->
                            signbutton.isClickable = true
                            progressbar.visibility = View.GONE
                            if (task.isSuccessful) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d("signup", "createUserWithEmail:success")
                                progressbar.visibility = View.GONE
                                Toast.makeText(this, "Sign up is success", Toast.LENGTH_SHORT).show()
                            }else{
                                // If sign in fails, display a message to the user.
                                Log.d("signup", "createUserWithEmail:failure", task.exception)
                                Toast.makeText(baseContext, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show()
                                progressbar.visibility = View.GONE }
                        }
            }else{Toast.makeText(this, "Passwords don't match", Toast.LENGTH_SHORT).show()}
        } else { Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show() }
    }



}