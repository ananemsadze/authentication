package com.example.authenticationactivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_in2.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import java.security.KeyStore

class SignInActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in2)
        init()

    }

    private fun init() {
        auth = Firebase.auth
        logbutton.setOnClickListener(){
            login()
        }

    }

    private fun emailchecker() {
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(emailenter.text.toString()).matches())

        else{ emailenter.setError("Email format is not correct")
        }
    }



    private fun login(){
        emailchecker()
        val foremail = emailenter.text.toString()
        val forpassword = passwordenter.text.toString()

        if (foremail.isNotEmpty() && forpassword.isNotEmpty()){
            progressbar2.visibility = View.VISIBLE
            logbutton.isClickable = false
            auth.signInWithEmailAndPassword(foremail, forpassword)
                .addOnCompleteListener(this) { task -> logbutton.isClickable = true
                    progressbar2.visibility = View.GONE
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("login", "signInWithEmail:success")
                        Toast.makeText(baseContext, "Authentication is success.",
                            Toast.LENGTH_SHORT).show()
                        val user = auth.currentUser
                        progressbar2.visibility = View.GONE


                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("login", "signInWithEmail:failure", task.exception)
                        Toast.makeText(baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                        progressbar2.visibility = View.GONE

                    }


                }

        }else{
            Toast.makeText(this, "Please fill all fields to sign in", Toast.LENGTH_SHORT).show()
        }
    }




}


