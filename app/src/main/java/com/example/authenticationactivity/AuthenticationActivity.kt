package com.example.authenticationactivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class AuthenticationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }




    private fun init(){
        signinbutton.setOnClickListener(){
            val intent = Intent (this, SignInActivity::class.java)
            startActivity(intent)

        }
        signupbutton.setOnClickListener(){
            val intent = Intent (this, SignUpActivity::class.java)
            startActivity(intent)

        }

    }

}